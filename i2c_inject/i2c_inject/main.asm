;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                       ;;
;;  I2C glitch injector for ATTiny85     ;;
;;                                       ;;
;;  (C) Copyright 2017 Jari Tulilahti    ;;
;;                                       ;;
;;  All right and deserved.              ;;
;;                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.nolist
.include "tn85def.inc"
.list

; Registers used as variables
;
.def counter	= r16		; Counter
.def running	= r17		; Is injector running
.def cancel	= r18		; Cancel button pressed
.def mode	= r19		; Injector mode
.def loop	= r20		; Looping register
.def temp	= r24		; Temporary register
.def temp2	= r25		; Temporary register 2

; Constants
;
.equ GLITCH		= 12	; Glitch inject length
.equ DELAY_BETWEEN	= 100	; Delay between glitches
.equ STOPCOUNT		= 250	; How many loops SDA and SCL
				; should be HIGH before inject

; Pins used for different signals
;
.equ PIN_SCL	= PB0
.equ PIN_SDA	= PB1
.equ PIN_LED	= PB2
.equ PIN_BUT1	= PB3
.equ PIN_BUT2	= PB4


;======================================================================
; Macros
;
.macro blink_on
	in temp, TIMSK
	sbr temp, (1 << OCIE1A)
	out TIMSK, temp
.endmacro

.macro blink_off
	in temp, TIMSK
	cbr temp, (1 << OCIE1A)
	out TIMSK, temp
	cbi PORTB, PIN_LED
.endmacro


.cseg
.org 0x00


;======================================================================
; Interrupt vectors
;
vectors:
	rjmp reset		; Start
	rjmp unused_int		; INT0_vect
	rjmp button		; PCINT0_vect
	rjmp ledflip		; TIMER1_COMPA_vect
	rjmp unused_int		; TIMER1_OVF_vect
	rjmp unused_int		; TIMER0_OVF_vect
	rjmp unused_int		; EE_RDY_vect
	rjmp unused_int		; ANA_COMP_vect
	rjmp unused_int		; ADC_vect
	rjmp unused_int		; TIMER1_COMPB_vect
	rjmp unused_int		; TIMER0_COMPA_vect
	rjmp unused_int		; TIMER0_COMPB_vect
	rjmp unused_int		; WDT_vect
	rjmp unused_int		; USI_START_vect
	rjmp unused_int		; USI_OVF_vect


;======================================================================
; Unknown vector
;
unused_int:
	rjmp unused_int


;======================================================================
; Jump here after reset, handle initialization
;
reset:
	; Set clock to 4MHz (divider /2)
	;
	ldi temp, (1 << CLKPCE)
	out CLKPR, temp
	ldi temp, (1 << CLKPS0)
	out CLKPR, temp

	; Set up stack pointer
	;
	ldi temp,low(RAMEND)
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp

	; Set variables (registers)
	;
	clr counter
	clr running
	clr cancel
	ldi mode, 3

	; Set up PINMUX
	;
	cbi DDRB, PIN_SCL	; input
	cbi DDRB, PIN_SDA	; input
	cbi DDRB, PIN_BUT1	; input
	cbi DDRB, PIN_BUT2	; input
	sbi DDRB, PIN_LED	; output
	cbi PORTB, PIN_SCL	; set LOW when output
	cbi PORTB, PIN_SDA	; set LOW when output
	cbi PORTB, PIN_LED	; turn LED off at boot

	; Set up Timer for LED flash
	; (CTC, /1024 divisor, counter 50, overflow)
	;
	ldi temp, (1 << CTC1) | (1 << CS10) | (1 << CS11) | (1 << CS13)
	out TCCR1, temp
	ldi temp, 100
	out OCR1C, temp

	; Set up Pin Change Interrupt for buttons
	;
	in temp, GIMSK
	sbr temp, (1 << PCIE)
	out GIMSK, temp
	in temp, PCMSK
	sbr temp, (1 << PIN_BUT1)
	out PCMSK, temp

	;
	; Enable interrupts
	sei

	rjmp main


;======================================================================
; Small delay
;
delay_s:
	push temp

delay_s_loop:
	dec temp
	brne delay_s_loop
	pop temp
	ret


;======================================================================
; Medium delay
;
delay_m:
	push temp
	push temp2
	ldi temp2, 255

delay_m_loop:
	nop
	nop
	nop
	nop
	dec temp2
	brne delay_m_loop
	dec temp
	brne delay_m_loop

	pop temp2
	pop temp
	ret


;======================================================================
; Handle button 1
;
button:
	sbic PINB, PIN_BUT1	; check if button down
	reti			; nope, exit
	in temp, SREG		; read SREG
	push temp		; push to stack

	sbrs running, 0		; is injector running?
	rjmp button_mode	; nope, select next mode
	ldi cancel, 1		; yes, cancel it
	clr running		; set running to zero

	rjmp button_blink

button_mode:
	inc mode		; next mode
	sbrc mode, 2		; is mode >3?
	ldi mode, 1		; reset to 1

button_blink:
	mov loop, mode		; loop blink led 'mode' times
	ldi temp, 255		; delay length
	cbi PORTB, PIN_LED	; LED off
	rcall delay_m		; delay

button_blinkloop:
	sbi PORTB, PIN_LED	; LED on
	rcall delay_m		; delay
	cbi PORTB, PIN_LED	; LED off
	rcall delay_m		; delay
	dec loop
	brne button_blinkloop
	rjmp button_end		; return

button_end:
	pop temp		; pop from stack
	out SREG, temp		; store back to SREG
	reti


;======================================================================
; Timer overflow to flip LED on/off
;
ledflip:
	sbi PINB, PIN_LED	; flip LED
	reti


;======================================================================
; Inject glitch into i2c signals
;
inject:
	ldi running, 1		; set running status
	blink_on		; start blinking LED

inject_waitcomm:
	; Wait SCL to go low
	;
	sbrc cancel, 0		; check if cancelled
	rjmp inject_cancel
	sbic PINB, PIN_SCL	; wait SCL to go low
	rjmp inject_waitcomm

	clr counter
inject_waitstop:
	; Wait for STOP signal
	;
	inc counter
	sbrc cancel, 0		; check if cancelled
	rjmp inject_cancel
	sbis PINB, PIN_SDA	; Check SDA high
	clr counter		; low, zero counter
	sbis PINB, PIN_SCL	; Check SCL high
	clr counter		; low, zero counter
	cpi counter, STOPCOUNT	; Wait for counter
	brlt inject_waitstop

	blink_off		; stop blinking LED

inject_glitch:
	; Inject the glitch itself
	;
	sbrc mode, 0
	sbi DDRB, PIN_SDA	; Inject SDA

	nop			; Slight delay between SDA & SCL
	nop			; this simulates START
	nop

	sbrc mode, 1
	sbi DDRB, PIN_SCL	; Inject SCL

	ldi temp, GLITCH	; glitch length
	rcall delay_s

	in temp, DDRB		; Stop glitches at same time
	cbr temp, (1 << PIN_SCL) | (1 << PIN_SDA)
	out DDRB, temp

	ldi temp, DELAY_BETWEEN	; delay between glitches
	rcall delay_m

	blink_on		; start blinking LED
	rjmp inject_waitcomm

inject_cancel:
	clr cancel		; clear cancel flag
	clr running		; clear running flag
	blink_off		; stop blinking LED

	ret

;======================================================================
; Main code
;
main:
	sbis PINB, PIN_BUT2
	rjmp inject
	rjmp main
